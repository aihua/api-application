package pairs

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/grzgajda/split_groups/employees"
)

func Test_IsDifferentAge(t *testing.T) {
	f := employees.CreateEmployee(1, "A", "B", "C", 25, true)
	s := employees.CreateEmployee(1, "A", "B", "C", 24, true)

	isTrue, _ := isDifferentAge(f, s)
	assert.True(t, isTrue)
}

func Test_BelongsToDifferentTeam(t *testing.T) {
	f := employees.CreateEmployee(1, "A", "B", "C", 25, true)
	s := employees.CreateEmployee(1, "B", "C", "C", 24, true)

	isTrue, _ := belongsToDifferentTeam(f, s)
	assert.True(t, isTrue)
}

func Test_IsLivingInAnotherDistrict(t *testing.T) {
	f := employees.CreateEmployee(1, "A", "B", "D", 25, true)
	s := employees.CreateEmployee(1, "B", "C", "C", 24, true)

	isTrue, _ := isLivingInAnotherDistrict(f, s)
	assert.True(t, isTrue)
}

func Test_BothNotHaveBadEyesight_OnlyOneHasBad(t *testing.T) {
	f := employees.CreateEmployee(1, "A", "B", "D", 25, false)
	s := employees.CreateEmployee(1, "B", "C", "C", 24, true)

	isTrue, _ := areBothNotHaveBadEyesight(f, s)
	assert.True(t, isTrue)
}

func Test_BothNotHaveBadEyesight_BothHaveGood(t *testing.T) {
	f := employees.CreateEmployee(1, "A", "B", "D", 25, false)
	s := employees.CreateEmployee(1, "B", "C", "C", 24, false)

	isTrue, _ := areBothNotHaveBadEyesight(f, s)
	assert.True(t, isTrue)
}

func Test_BothNotHaveBadEyesight_BothCannotHaveBad(t *testing.T) {
	f := employees.CreateEmployee(1, "A", "B", "D", 25, true)
	s := employees.CreateEmployee(1, "B", "C", "C", 24, true)

	isTrue, _ := areBothNotHaveBadEyesight(f, s)
	assert.False(t, isTrue)
}

func Test_Composition(t *testing.T) {
	f := employees.CreateEmployee(1, "A", "B", "D", 25, true)
	s := employees.CreateEmployee(1, "B", "C", "E", 24, false)

	isTrue, howMany := composeSpecifications(
		isDifferentAge,
		belongsToDifferentTeam,
		isLivingInAnotherDistrict,
		areBothNotHaveBadEyesight,
	)(f, s)
	assert.True(t, isTrue)
	assert.Equal(t, 4, howMany)
}
