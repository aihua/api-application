package main

import (
	"time"

	"gitlab.com/grzgajda/split_groups/pairs"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/grzgajda/split_groups/employees"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	db := startDB(0)
	db.LogMode(true)

	corsConfig := cors.DefaultConfig()
	corsConfig.AddAllowMethods("DELETE")
	corsConfig.AllowAllOrigins = true

	server := gin.Default()
	server.Use(cors.New(corsConfig))
	server.Use(allowOriginHeader())

	employeesRepo := employees.CreateRepository(db)
	solver := pairs.CreateSolver()

	groupEmployeesRoutes(server.Group("/employees"), employeesRepo)
	server.GET("/pairs/", ConnectPairs(employeesRepo.FindAll, solver))

	server.Run(":3000")
}

func groupEmployeesRoutes(router *gin.RouterGroup, repo employees.Repository) {
	router.GET("/", GetEmployeesHandler(repo.FindAll))
	router.GET("/:id", GetSingleEmployeeHandler(repo.Find))
	router.POST("/", SaveEmployeeHandler(repo.Persist))
	router.PUT("/:id", UpdateEmployeeHandler(repo.Find, repo.Persist))
	router.DELETE("/:id", RemoveEmployeeHandler(repo.Find, repo.Remove))
}

func allowOriginHeader() func(c *gin.Context) {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

func startDB(counter int) *gorm.DB {
	db, err := gorm.Open("mysql", "app_user:app_pass@tcp(db)/app_base?parseTime=true")
	if err != nil && counter > 5 {
		panic(err)
	} else if err != nil {
		time.Sleep(1 * time.Second)

		return startDB(counter + 1)
	}

	return db
}
