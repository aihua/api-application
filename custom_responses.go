package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func internalServerError(err error) (statusCode int, body gin.H) {
	return http.StatusInternalServerError, gin.H{"error_message": err.Error()}
}

func badRequestError(err error) (statusCode int, body gin.H) {
	return http.StatusBadRequest, gin.H{"error_message": err.Error()}
}

func emptyResponse() int {
	return http.StatusNoContent
}

func okWithData(data interface{}) (statusCode int, body gin.H) {
	return http.StatusOK, gin.H{"data": data}
}
