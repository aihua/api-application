package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/grzgajda/split_groups/pairs"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/grzgajda/split_groups/employees"
)

type fakeSolve struct{}

func (s fakeSolve) Solve(all []employees.Employee) []pairs.Pair {
	return []pairs.Pair{
		pairs.Pair{First: employees.Employee{}, Second: employees.Employee{}},
	}
}

type emptySolve struct{}

func (s emptySolve) Solve(all []employees.Employee) []pairs.Pair {
	return []pairs.Pair{}
}

func Test_ConnectPairs_OK(t *testing.T) {
	findAll := func() ([]employees.Employee, error) {
		return []employees.Employee{}, nil
	}

	r := gin.New()
	r.GET("/", errorTestingContext(t), ConnectPairs(findAll, fakeSolve{}))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)
}

func Test_ConnectPairs_EmptyEmployees(t *testing.T) {
	findAll := func() ([]employees.Employee, error) {
		return []employees.Employee{}, fmt.Errorf("empty content")
	}

	r := gin.New()
	r.GET("/", errorTestingContext(t), ConnectPairs(findAll, fakeSolve{}))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusNoContent, w.Code)
}

func Test_ConnectPairs_EmptyContent(t *testing.T) {
	findAll := func() ([]employees.Employee, error) {
		return []employees.Employee{}, nil
	}

	r := gin.New()
	r.GET("/", errorTestingContext(t), ConnectPairs(findAll, emptySolve{}))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodGet, "/", nil)
	req.Header.Add("Content-Type", "application/json")

	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusNoContent, w.Code)
}
