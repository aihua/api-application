package employees

import "time"

type Employee struct {
	ID           int        `json:"id" gorm:"primary_key"`
	Name         string     `json:"name" binding:"required" gorm:"type:varchar(100)"`
	Age          int        `json:"age" binding:"required"`
	TeamName     string     `json:"team" binding:"required" gorm:"type:varchar(100)"`
	DistrictName string     `json:"district" binding:"required" gorm:"type:varchar(100)"`
	Eyesight     bool       `json:"badEyesight"`
	CreatedAt    time.Time  `json:"-"`
	UpdatedAt    time.Time  `json:"-"`
	DeletedAt    *time.Time `json:"-" sql:"index"`
}

func CreateEmployee(id int, name, teamName, districtName string, age int, eyesight bool) Employee {
	return Employee{
		ID:           id,
		Name:         name,
		TeamName:     teamName,
		DistrictName: districtName,
		Age:          age,
		Eyesight:     eyesight,
	}
}
