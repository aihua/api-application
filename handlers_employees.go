package main

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/grzgajda/split_groups/employees"
)

func GetEmployeesHandler(findAll employees.FindAll) gin.HandlerFunc {
	return func(c *gin.Context) {
		employees, err := findAll()
		if err != nil {
			c.JSON(internalServerError(c.Error(err)))
			return
		}

		c.JSON(okWithData(employees))
	}
}

func GetSingleEmployeeHandler(find employees.Find) gin.HandlerFunc {
	return func(c *gin.Context) {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			c.JSON(badRequestError(c.Error(err)))
			return
		}

		employee, err := find(id)
		if err != nil {
			c.JSON(internalServerError(c.Error(err)))
			return
		}

		c.JSON(okWithData(employee))
	}
}

func SaveEmployeeHandler(save employees.Persist) gin.HandlerFunc {
	return func(c *gin.Context) {
		var employee employees.Employee
		if err := c.ShouldBindJSON(&employee); err != nil {
			c.JSON(internalServerError(c.Error(err)))
			return
		}

		if err := save(employee); err != nil {
			c.JSON(internalServerError(c.Error(err)))
		} else {
			c.Status(http.StatusCreated)
		}
	}
}

func UpdateEmployeeHandler(find employees.Find, save employees.Persist) gin.HandlerFunc {
	return func(c *gin.Context) {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			c.JSON(badRequestError(c.Error(err)))
			return
		}

		employee, err := find(id)
		if err != nil {
			c.JSON(internalServerError(c.Error(err)))
			return
		}

		if err := c.ShouldBindJSON(&employee); err != nil {
			c.JSON(internalServerError(c.Error(err)))
			return
		}

		if err := save(employee); err != nil {
			c.JSON(internalServerError(c.Error(err)))
		} else {
			c.Status(http.StatusCreated)
		}
	}
}

func RemoveEmployeeHandler(find employees.Find, remove employees.Remove) gin.HandlerFunc {
	return func(c *gin.Context) {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			c.JSON(badRequestError(c.Error(err)))
			return
		}

		employee, err := find(id)
		if err != nil {
			c.JSON(internalServerError(c.Error(err)))
			return
		}

		if err := remove(employee); err != nil {
			c.JSON(internalServerError(c.Error(err)))
		} else {
			c.Status(http.StatusAccepted)
		}
	}
}
